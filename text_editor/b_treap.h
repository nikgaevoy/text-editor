#pragma once

#include "pch.h"

template<size_t bound = 1, class URNG = std::mt19937, URNG &rng = mt>
class b_treap
{
    struct node
    {
        static_assert (bound > 0, "Node buffer size should be positive");

        using nodeptr = std::shared_ptr<const node>;

        std::string x;
        size_t siz;
        nodeptr l, r;

        node (const std::string &val, nodeptr l = nullptr, nodeptr r = nullptr);
        node (const std::string &val, size_t pos, size_t len = std::string::npos,
              nodeptr l = nullptr, nodeptr r = nullptr);
        node (const char *s, nodeptr l = nullptr, nodeptr r = nullptr);
        node (const char *s, size_t n, nodeptr l = nullptr, nodeptr r = nullptr);
    };

    using nodeptr = std::shared_ptr<const node>;

    static size_t get_size (nodeptr h);
    static nodeptr front (nodeptr h);
    static nodeptr back (nodeptr h);
    static nodeptr pop_front (nodeptr h);
    static nodeptr pop_back (nodeptr h);
    static void to_string (nodeptr h, std::string &str);
    static nodeptr tree_merge (nodeptr le, nodeptr ri);
    static void tree_split (nodeptr h, nodeptr &le, nodeptr &mil, nodeptr &mir, nodeptr &ri, size_t wh);
    static void node_split (nodeptr h, nodeptr &le, nodeptr &ri, size_t wh);
    static nodeptr merge (nodeptr le, nodeptr ri);
    static void split (nodeptr h, nodeptr &le, nodeptr &ri, size_t wh);

    nodeptr root;

    b_treap (nodeptr root);

    static void output (nodeptr root, std::ostream &cout)
    {
        if (root == nullptr)
            return;

        output (root->l, cout);
        cout << '(' << root->x << ", " << root->siz << ")\t";
        output (root->r, cout);
    }

public:
    b_treap ();
    b_treap (const std::string &val);
    b_treap (const std::string &val, size_t pos, size_t len = std::string::npos);
    b_treap (const char *s);
    b_treap (const char *s, size_t n);
    void swap (b_treap<bound, URNG, rng> &val);
    size_t size () const;
    std::string to_string () const;
    b_treap substr (size_t l = 0, size_t r = std::numeric_limits<size_t>::max ()) const;
    static std::pair<b_treap, b_treap> split (size_t wh);
    b_treap insert (size_t wh, const b_treap<bound, URNG, rng> &val);
    static b_treap merge (const b_treap &le, const b_treap &ri);
    b_treap operator+ (const b_treap<bound, URNG, rng> &val) const;
    b_treap &operator+= (const b_treap<bound, URNG, rng> &val);
    void output (std::ostream &cout = std::cerr) const
    {
        output (root, cout);
        cout << std::endl;
    }
};


#include "b_treap.ipp"
