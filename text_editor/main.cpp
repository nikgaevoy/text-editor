#include "pch.h"
#include "b_treap.h"
#include "treap.h"


using namespace std;


void solve (istream &cin = std::cin, ostream &cout = std::cout)
{
    treap<int> tr = {1, 2, 3, 0};

    tr.output (cout);

    return;

    string str;
    int n;

    cin >> str >> n;

    vector<b_treap<3>> ver = {str};

    for (int i = 0; i < n; i++)
    {
        char c;

        cin >> c;

        if (c == '+')
        {
            int t;

            cin >> t;

            cout << t << endl;

            ver.push_back (ver[i] + ver[t]);
        }
        else
        {
            assert (c == 's');

            int a, b;

            cin >> a >> b;

            a %= (ver.back ().size () + 1);
            b %= (ver.back ().size () + 1);

            if (b < a)
                swap (a, b);

            cout << a << ' ' << b << endl;

            ver.push_back (ver[i].substr (a, b));
        }

        cout << i + 1 << ")\t" << ver.back ().to_string () << endl;
    }
}


void stress (istream &cin = std::cin, ostream &cout = std::cout)
{
    string str;
    int n;

    cin >> str >> n;

    vector<string> ver = {str};

    for (int i = 0; i < n; i++)
    {
        char c;

        cin >> c;

        if (c == '+')
        {
            int t;

            cin >> t;

            cout << t << endl;

            ver.push_back (ver[i] + ver[t]);
        }
        else
        {
            assert (c == 's');
            
            int a, b;

            cin >> a >> b;

            a %= (ver.back ().size () + 1);
            b %= (ver.back ().size () + 1);

            if (b < a)
                swap (a, b);

            cout << a << ' ' << b << endl;

            ver.push_back (ver[i].substr (a, b - a));
        }

        cout << i + 1 << ")\t" << ver.back () << endl;
    }
}


void gen (ostream &cout = std::cout)
{
    mt19937 mt (736);

    string str (26, 'a');

    iota (str.begin (), str.end (), 'a');

    cout << str << endl;

    const int n = 1 << 16;

    cout << n << endl;

    for (int i = 0; i < n; i++)
    {
        uniform_int_distribution<int> op (0, 1);

        if (op (mt) == 0)
        {
            uniform_int_distribution<int> uid (0, i);

            cout << "+ " << uid (mt) << endl;
        }
        else
        {
            uniform_int_distribution<int> uid (0, 32736);

            cout << "s " << uid (mt) << ' ' << uid (mt) << endl;
        }
    }
}


int main ()
{
    solve ();

    return 0;

    stringstream ss, in_solve, in_stress, out_solve, out_stress;

    gen (ss);

    in_solve << ss.str ();
    in_stress << ss.str ();

    solve (in_solve, out_solve);
    stress (in_stress, out_stress);

    if (out_solve.str () == out_stress.str ())
        cout << "Ok" << endl;
    else
    {
        cout << "Fail" << endl;

        cout << ss.str () << endl;
        cout << out_solve.str () << endl;
        cout << out_stress.str () << endl;
    }

    return 0;
}

