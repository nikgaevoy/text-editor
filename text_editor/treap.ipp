#include "pch.h"
#include "treap.h"


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng>::node::node (const T &x, const nodeptr l, const nodeptr r) :
    x (x), l (l), r (r), siz (get_size (l) + 1 + get_size (r))
{}


template<class T, class URNG, URNG &rng>
size_t treap<T, URNG, rng>::get_size (nodeptr h)
{
    return h == nullptr ? 0 : h->siz;
}


template<class T, class URNG, URNG & rng>
inline typename treap<T, URNG, rng>::nodeptr treap<T, URNG, rng>::build (const std::initializer_list<T> &il, size_t l, size_t r)
{
    if (r <= l)
        return nullptr;

    auto t = (l + r) / 2;

    return std::make_shared<node> (*(il.begin () + t), build (il, l, t), build (il, t + 1, r));
}


template<class T, class URNG, URNG &rng>
inline typename treap<T, URNG, rng>::nodeptr treap<T, URNG, rng>::merge (nodeptr le, nodeptr ri)
{
    if (le == nullptr)
        return ri;
    if (ri == nullptr)
        return le;

    std::uniform_int_distribution<size_t> uid (0, get_size (le) + get_size (ri) - 1);

    if (uid (rng) < get_size (le))
        return std::make_shared<node> (le->x, le->l, merge (le->r, ri));
    else
        return std::make_shared<node> (ri->x, merge (le, ri->l), ri->r);
}


template<class T, class URNG, URNG &rng>
inline void treap<T, URNG, rng>::split (nodeptr h, nodeptr & le, nodeptr & ri, size_t wh)
{
    if (wh == 0)
    {
        le = nullptr;
        ri = h;

        return;
    }
    if (wh >= get_size (h))
    {
        le = h;
        ri = nullptr;

        return;
    }

    if (get_size (h->l) < wh)
    {
        le = copy (h);
        split (h->r, le->r, ri, wh - get_size (h->l) - 1);
    }
    else
    {
        ri = copy (h);
        split (h->l, le, ri->l, wh);
    }
}


template<class T, class URNG, URNG &rng>
inline typename treap<T, URNG, rng>::nodeptr treap<T, URNG, rng>::erase (nodeptr h, size_t wh)
{
    assert (h != nullptr);

    if (get_size (h->l) == wh)
        return merge (h->l, h->r);
    if (wh < get_size (h->l))
        return std::make_shared<node> (h->x, erase (h->l, wh), h->r);
    else
        return std::make_shared<node> (h->x, h->l, erase (h->r, wh - get_size (h->l) - 1));
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng>::treap (nodeptr root) : root (root)
{}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng>::treap () : root (nullptr)
{}


template<class T, class URNG, URNG & rng>
inline treap<T, URNG, rng>::treap (const std::initializer_list<T> &il) : root (build (il, 0, il.size ()))
{}


template<class T, class URNG, URNG &rng>
inline void treap<T, URNG, rng>::swap (treap<T, URNG, rng> &val)
{
    std::swap (root, val.root);
}


template<class T, class URNG, URNG &rng>
inline size_t treap<T, URNG, rng>::size () const
{
    return get_size (root);
}


template<class T, class URNG, URNG &rng>
inline std::pair<treap<T, URNG, rng>, treap<T, URNG, rng>> treap<T, URNG, rng>::split (size_t wh)
{
    nodeptr le, ri;

    split (root, le, ri, wh);

    return {treap (le), treap (ri)};
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng> treap<T, URNG, rng>::insert (size_t wh, const treap<T, URNG, rng> &val) const
{
    nodeptr le, ri;

    split (root, le, ri, wh);

    return treap (merge (merge (le, val), ri));
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng> treap<T, URNG, rng>::erase (size_t wh) const
{
    return treap (erase (root, wh));
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng> treap<T, URNG, rng>::merge (const treap &le, const treap &ri)
{
    return treap (merge (le.root, ri.root));
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng> treap<T, URNG, rng>::operator+ (const treap<T, URNG, rng> &val) const
{
    return treap (merge (root, val.root));
}


template<class T, class URNG, URNG &rng>
inline treap<T, URNG, rng> &treap<T, URNG, rng>::operator+= (const treap<T, URNG, rng> &val)
{
    root = merge (root, val.root);

    return *this;
}
