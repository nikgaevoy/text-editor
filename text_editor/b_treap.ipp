#include "pch.h"
#include "b_treap.h"


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::node::node (const std::string &val, nodeptr l, nodeptr r) :
    x (val), l (l), r (r), siz (get_size (l) + val.size () + get_size (r))
{}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::node::node (const std::string &val, size_t pos, size_t len, nodeptr l, nodeptr r) :
    x (val.substr (pos, len)), l (l), r (r), siz (get_size (l) + x.size () + get_size (r))
{}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::node::node (const char *s, nodeptr l, nodeptr r) :
    x (s), l (l), r (r), siz (get_size (l) + x.size () + get_size (r))
{}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::node::node (const char *s, size_t n, nodeptr l, nodeptr r) :
    x (s, n), l (l), r (r), siz (get_size (l) + x.size () + get_size (r))
{}


template<size_t bound, class URNG, URNG &rng>
inline size_t b_treap<bound, URNG, rng>::get_size (nodeptr h)
{
    return h == nullptr ? 0 : h->siz;
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::front (nodeptr h)
{
    if (h == nullptr)
        return nullptr;

    while (h->l != nullptr)
        h = h->l;

    return h;
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::back (nodeptr h)
{
    if (h == nullptr)
        return nullptr;

    while (h->r != nullptr)
        h = h->r;

    return h;
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::pop_front (nodeptr h)
{
    assert (h != nullptr);

    return h->l == nullptr ? h->r : std::make_shared<node> (h->x, pop_front (h->l), h->r);
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::pop_back (nodeptr h)
{
    assert (h != nullptr);

    return h->r == nullptr ? h->l : std::make_shared<node> (h->x, h->l, pop_back (h->r));
}


template<size_t bound, class URNG, URNG &rng>
inline void b_treap<bound, URNG, rng>::to_string (nodeptr h, std::string &str)
{
    if (h == nullptr)
        return;

    to_string (h->l, str);
    str += h->x;
    to_string (h->r, str);
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::tree_merge (nodeptr le, nodeptr ri)
{
    if (le == nullptr)
        return ri;
    if (ri == nullptr)
        return le;

    std::uniform_int_distribution<size_t> uid (0, get_size (le) + get_size (ri) - 1);

    if (uid (rng) < get_size (le))
        return std::make_shared<node> (le->x, le->l, tree_merge (le->r, ri));
    else
        return std::make_shared<node> (ri->x, tree_merge (le, ri->l), ri->r);
}


template<size_t bound, class URNG, URNG &rng>
inline void b_treap<bound, URNG, rng>::tree_split (nodeptr h, nodeptr &le, nodeptr &mil, nodeptr &mir, nodeptr &ri, size_t wh)
{
    if (wh == 0)
    {
        le = nullptr;
        ri = h;

        return;
    }
    if (wh >= get_size (h))
    {
        le = h;
        ri = nullptr;

        return;
    }

    if (get_size (h->l) < wh)
    {
        if (wh < get_size (h->l) + h->x.size ())
        {
            node_split (h, mil, mir, wh - get_size (h->l));
            le = h->l;
            ri = h->r;

            return;
        }

        nodeptr tmp;
        tree_split (h->r, tmp, mil, mir, ri, wh - get_size (h->l) - h->x.size ());
        le = std::make_shared<node> (h->x, h->l, tmp);
    }
    else
    {
        nodeptr tmp;
        tree_split (h->l, le, mil, mir, tmp, wh);
        ri = std::make_shared<node> (h->x, tmp, h->r);
    }
}


template<size_t bound, class URNG, URNG &rng>
inline void b_treap<bound, URNG, rng>::node_split (nodeptr h, nodeptr &le, nodeptr &ri, size_t wh)
{
    assert (h != nullptr);

    assert (wh <= h->x.size ());

    if (wh == 0)
        le = nullptr;
    else
        le = std::make_shared<node> (h->x.substr (0, wh));

    if (wh == h->x.size ())
        ri = nullptr;
    else
        ri = std::make_shared<node> (h->x.substr (wh));
}


template<size_t bound, class URNG, URNG &rng>
inline typename b_treap<bound, URNG, rng>::nodeptr b_treap<bound, URNG, rng>::merge (nodeptr le, nodeptr ri)
{
    if (le == nullptr)
        return ri;
    if (ri == nullptr)
        return le;

    nodeptr mil = back (le);
    nodeptr mir = front (ri);

    if (mil->x.size () + mir->x.size () <= bound)
    {
        nodeptr mi = std::make_shared<node> (mil->x + mir->x);

        le = pop_back (le);
        ri = pop_front (ri);

        return tree_merge (tree_merge (le, mi), ri);
    }

    return tree_merge (le, ri);
}


template<size_t bound, class URNG, URNG &rng>
inline void b_treap<bound, URNG, rng>::split (nodeptr h, nodeptr &le, nodeptr &ri, size_t wh)
{
    nodeptr mil = nullptr;
    nodeptr mir = nullptr;

    tree_split (h, le, mil, mir, ri, wh);

    le = merge (le, mil);
    ri = merge (mir, ri);
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap (nodeptr root) : root (root)
{}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap () : root (nullptr)
{}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap (const std::string &val) : root (nullptr)
{
    for (size_t pos = 0; pos < val.size (); pos += bound)
        root = tree_merge (root, std::make_shared<node> (val, pos, bound));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap (const std::string &val, size_t pos, size_t len) :
    root (nullptr)
{
    for (size_t off = 0; off < len && pos + off; off += bound)
        root = tree_merge (root, std::make_shared<node> (val, pos + off, bound));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap (const char *s) : root (nullptr)
{
    auto n = std::strlen (s);

    for (size_t off = 0; off < n; off += bound)
        root = tree_merge (root, std::make_shared<node> (s + off, std::min (n - off, bound)));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng>::b_treap (const char *s, size_t n) : root (nullptr)
{
    for (size_t off = 0; off < n; off += bound)
        root = tree_merge (root, std::make_shared<node> (s + off, std::min (n - off, bound)));
}


template<size_t bound, class URNG, URNG &rng>
inline void b_treap<bound, URNG, rng>::swap (b_treap<bound, URNG, rng> &val)
{
    std::swap (root, val.root);
}


template<size_t bound, class URNG, URNG &rng>
inline size_t b_treap<bound, URNG, rng>::size () const
{
    return get_size (root);
}


template<size_t bound, class URNG, URNG &rng>
inline std::string b_treap<bound, URNG, rng>::to_string () const
{
    std::string ret;

    to_string (root, ret);

    return ret;
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng> b_treap<bound, URNG, rng>::substr (size_t l, size_t r) const
{
    if (r <= l)
        return b_treap<bound, URNG, rng> ();

    nodeptr le, mi, ri;

    split (root, le, ri, r);
    split (le, le, mi, l);

    return b_treap<bound, URNG, rng> (mi);
}


template<size_t bound, class URNG, URNG &rng>
inline std::pair<b_treap<bound, URNG, rng>, b_treap<bound, URNG, rng>> b_treap<bound, URNG, rng>::split (size_t wh)
{
    nodeptr le, ri;

    split (root, le, ri, wh);

    return {b_treap (le), b_treap (ri)};
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng> b_treap<bound, URNG, rng>::insert (size_t wh, const b_treap &val)
{
    nodeptr le, ri;

    split (root, le, ri, wh);

    return b_treap (merge (merge (le, val.root), ri));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng> b_treap<bound, URNG, rng>::merge (const b_treap &le, const b_treap &ri)
{
    return b_treap (merge (le.root, ri.root));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng> b_treap<bound, URNG, rng>::operator+ (const b_treap<bound, URNG, rng> &val) const
{
    return b_treap (merge (root, val.root));
}


template<size_t bound, class URNG, URNG &rng>
inline b_treap<bound, URNG, rng> &b_treap<bound, URNG, rng>::operator+= (const b_treap<bound, URNG, rng> &val)
{
    root = merge (root, val.root);

    return *this;
}
