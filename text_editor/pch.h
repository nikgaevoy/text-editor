#pragma once

#ifndef PCH_H
#define PCH_H

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <memory>
#include <functional>
#include <random>
#include <chrono>
#include <sstream>
#include <numeric>

#include <cassert>

static std::mt19937 mt (736);

#endif //PCH_H

