#pragma once

#include "pch.h"

template<class T, class URNG = std::mt19937, URNG &rng = mt>
class treap
{
    struct node
    {
        using nodeptr = std::shared_ptr<const node>;

        T x;
        size_t siz;
        nodeptr l, r;

        node (const T &x, nodeptr l = nullptr, nodeptr r = nullptr);
    };

    using nodeptr = std::shared_ptr<const node>;

    static size_t get_size (nodeptr h);
    static nodeptr build (const std::initializer_list<T> &il, size_t l, size_t r);
    static nodeptr merge (nodeptr le, nodeptr ri);
    static void split (nodeptr h, nodeptr &le, nodeptr &ri, size_t wh);
    static nodeptr erase (nodeptr h, size_t wh);

    nodeptr root;

    treap (nodeptr root);


    static void output (nodeptr root, std::ostream &cout)
    {
        if (root == nullptr)
            return;

        output (root->l, cout);
        cout << '(' << root->x << ", " << root->siz << ")\t";
        output (root->r, cout);
    }

public:
    treap ();
    treap (const std::initializer_list<T> &il);
    void swap (treap<T, URNG, rng> &val);
    size_t size () const;
    static std::pair<treap, treap> split (size_t wh);
    treap insert (size_t wh, const treap<T, URNG, rng> &val) const;
    treap erase (size_t wh) const;
    static treap merge (const treap &le, const treap &ri);
    treap operator+ (const treap<T, URNG, rng> &val) const;
    treap &operator+= (const treap<T, URNG, rng> &val);
    void output (std::ostream &cout = std::cerr) const
    {
        output (root, cout);
        cout << std::endl;
    }
};


#include "treap.ipp"
